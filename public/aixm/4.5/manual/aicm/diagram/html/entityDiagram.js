 
	var EntityDiagrams = new Array();
	
		EntityDiagrams['ADDRESS'] = new Array();
	
		EntityDiagrams['AD_HP'] = new Array();
	
		EntityDiagrams['AD_HP_COLLOCATION'] = new Array();
	
		EntityDiagrams['AD_HP_GND_SER'] = new Array();
	
		EntityDiagrams['AD_HP_NAV_AID'] = new Array();
	
		EntityDiagrams['AD_HP_OBSTACLE'] = new Array();
	
		EntityDiagrams['AD_HP_SURFACE_BORDER'] = new Array();
	
		EntityDiagrams['AD_HP_SURFACE_VERTEX'] = new Array();
	
		EntityDiagrams['AD_HP_USAGE'] = new Array();
	
		EntityDiagrams['AD_HP_USAGE_CONDITION'] = new Array();
	
		EntityDiagrams['AD_HP_USAGE_LIMITATION'] = new Array();
	
		EntityDiagrams['AERO_GND_LGT'] = new Array();
	
		EntityDiagrams['AIRCRAFT_CLASS'] = new Array();
	
		EntityDiagrams['AIRSPACE'] = new Array();
	
		EntityDiagrams['AIRSPACE_AGGREG_COMP'] = new Array();
	
		EntityDiagrams['AIRSPACE_ASSOCIATION'] = new Array();
	
		EntityDiagrams['AIRSPACE_BORDER'] = new Array();
	
		EntityDiagrams['AIRSPACE_BORDER_CROSSING'] = new Array();
	
		EntityDiagrams['AIRSPACE_BORDER_VERTEX'] = new Array();
	
		EntityDiagrams['AIRSPACE_CIRCLE_VERTEX'] = new Array();
	
		EntityDiagrams['AIRSPACE_CLINE_VERTEX'] = new Array();
	
		EntityDiagrams['AIRSPACE_CORRIDOR'] = new Array();
	
		EntityDiagrams['AIRSPACE_DERIV_GEOMETRY'] = new Array();
	
		EntityDiagrams['AIRSPACE_VERTEX'] = new Array();
	
		EntityDiagrams['ANGLE_INDICATION'] = new Array();
	
		EntityDiagrams['APRON'] = new Array();
	
		EntityDiagrams['APRON_LGT_SYS'] = new Array();
	
		EntityDiagrams['AUTH_FOR_AIRSPACE'] = new Array();
	
		EntityDiagrams['CALLSIGN_DETAIL'] = new Array();
	
		EntityDiagrams['DESIGNATED_POINT'] = new Array();
	
		EntityDiagrams['DIRECT_FLIGHT'] = new Array();
	
		EntityDiagrams['DIRECT_FLIGHT_CLASS'] = new Array();
	
		EntityDiagrams['DIRECT_FLIGHT_SEGMENT'] = new Array();
	
		EntityDiagrams['DISTANCE_INDICATION'] = new Array();
	
		EntityDiagrams['DME'] = new Array();
	
		EntityDiagrams['DME_USAGE_LIMIT'] = new Array();
	
		EntityDiagrams['EN_ROUTE_RTE'] = new Array();
	
		EntityDiagrams['FATO'] = new Array();
	
		EntityDiagrams['FATO_CLINE_POINT'] = new Array();
	
		EntityDiagrams['FATO_DIRECTION'] = new Array();
	
		EntityDiagrams['FATO_DIRECTION_ALS'] = new Array();
	
		EntityDiagrams['FATO_DIRECTION_DECL_DIST'] = new Array();
	
		EntityDiagrams['FATO_DIRECTION_LGT_SYS'] = new Array();
	
		EntityDiagrams['FATO_DIRECTION_OBSTACLE'] = new Array();
	
		EntityDiagrams['FATO_DIRECTION_STAR'] = new Array();
	
		EntityDiagrams['FATO_PROTECT_AREA'] = new Array();
	
		EntityDiagrams['FLIGHT_CLASS'] = new Array();
	
		EntityDiagrams['FLOW_COND_COMBINATION'] = new Array();
	
		EntityDiagrams['FLOW_COND_ELEMENT'] = new Array();
	
		EntityDiagrams['FLOW_COND_ELEMENT_LVL'] = new Array();
	
		EntityDiagrams['FREQUENCY'] = new Array();
	
		EntityDiagrams['FUEL'] = new Array();
	
		EntityDiagrams['GATE_STAND'] = new Array();
	
		EntityDiagrams['GEO_BORDER'] = new Array();
	
		EntityDiagrams['GEO_BORDER_VERTEX'] = new Array();
	
		EntityDiagrams['HOLDING_PROCEDURE'] = new Array();
	
		EntityDiagrams['IAP'] = new Array();
	
		EntityDiagrams['IAP_USAGE'] = new Array();
	
		EntityDiagrams['ILS'] = new Array();
	
		EntityDiagrams['ILS_GP'] = new Array();
	
		EntityDiagrams['ILS_LLZ'] = new Array();
	
		EntityDiagrams['LANDING_PROTECTION_AREA'] = new Array();
	
		EntityDiagrams['MKR'] = new Array();
	
		EntityDiagrams['MLS'] = new Array();
	
		EntityDiagrams['MLS_AZIMUTH'] = new Array();
	
		EntityDiagrams['MLS_ELEVATION'] = new Array();
	
		EntityDiagrams['MSA'] = new Array();
	
		EntityDiagrams['MSA_GROUP'] = new Array();
	
		EntityDiagrams['NAVAID_LIMITATION'] = new Array();
	
		EntityDiagrams['NAV_SYS_CHECKPOINT'] = new Array();
	
		EntityDiagrams['NDB'] = new Array();
	
		EntityDiagrams['NDB_USAGE_LIMIT'] = new Array();
	
		EntityDiagrams['NITROGEN'] = new Array();
	
		EntityDiagrams['OBSTACLE'] = new Array();
	
		EntityDiagrams['OCA_OCH'] = new Array();
	
		EntityDiagrams['OIL'] = new Array();
	
		EntityDiagrams['ORG_AUTH'] = new Array();
	
		EntityDiagrams['ORG_AUTH_ASSOC'] = new Array();
	
		EntityDiagrams['OXYGEN'] = new Array();
	
		EntityDiagrams['PASSENGER_FACILITY'] = new Array();
	
		EntityDiagrams['PREDEFINED_LVL'] = new Array();
	
		EntityDiagrams['PREDEFINED_LVL_COLUMN'] = new Array();
	
		EntityDiagrams['PREDEFINED_LVL_TABLE'] = new Array();
	
		EntityDiagrams['PROCEDURE_LEG'] = new Array();
	
		EntityDiagrams['RTE_PORTION'] = new Array();
	
		EntityDiagrams['RTE_SEG'] = new Array();
	
		EntityDiagrams['RTE_SEG_USE'] = new Array();
	
		EntityDiagrams['RTE_SEG_USE_LVL'] = new Array();
	
		EntityDiagrams['RWY'] = new Array();
	
		EntityDiagrams['RWY_CLINE_POINT'] = new Array();
	
		EntityDiagrams['RWY_DIRECTION'] = new Array();
	
		EntityDiagrams['RWY_DIRECTION_ALS'] = new Array();
	
		EntityDiagrams['RWY_DIRECTION_DECL_DIST'] = new Array();
	
		EntityDiagrams['RWY_DIRECTION_LGT_SYS'] = new Array();
	
		EntityDiagrams['RWY_DIRECTION_OBSTACLE'] = new Array();
	
		EntityDiagrams['RWY_DIRECTION_STAR'] = new Array();
	
		EntityDiagrams['RWY_PROTECT_AREA'] = new Array();
	
		EntityDiagrams['SEGMENT'] = new Array();
	
		EntityDiagrams['SERVICE'] = new Array();
	
		EntityDiagrams['SERVICE_AT_AD_HP'] = new Array();
	
		EntityDiagrams['SERVICE_IN_AIRSPACE'] = new Array();
	
		EntityDiagrams['SERVICE_ON_HOLDING_PROC'] = new Array();
	
		EntityDiagrams['SERVICE_ON_IAP'] = new Array();
	
		EntityDiagrams['SERVICE_ON_RTE_SEG'] = new Array();
	
		EntityDiagrams['SERVICE_ON_SID'] = new Array();
	
		EntityDiagrams['SERVICE_ON_STAR'] = new Array();
	
		EntityDiagrams['SID'] = new Array();
	
		EntityDiagrams['SID_USAGE'] = new Array();
	
		EntityDiagrams['SIGNIFICANT_POINT'] = new Array();
	
		EntityDiagrams['SIGNIFICANT_POINT_IN_AS'] = new Array();
	
		EntityDiagrams['SPECIAL_DATE'] = new Array();
	
		EntityDiagrams['SPEC_NAV_STATION'] = new Array();
	
		EntityDiagrams['SPEC_NAV_SYS'] = new Array();
	
		EntityDiagrams['STAR'] = new Array();
	
		EntityDiagrams['STAR_USAGE'] = new Array();
	
		EntityDiagrams['SURFACE_CHARACTERISTICS'] = new Array();
	
		EntityDiagrams['SURFACE_LGT_GROUP'] = new Array();
	
		EntityDiagrams['SURFACE_LGT_SYS'] = new Array();
	
		EntityDiagrams['SWY'] = new Array();
	
		EntityDiagrams['TACAN'] = new Array();
	
		EntityDiagrams['TACAN_USAGE_LIMIT'] = new Array();
	
		EntityDiagrams['TFC_FLOW_RESTR'] = new Array();
	
		EntityDiagrams['TFC_FLOW_RTE'] = new Array();
	
		EntityDiagrams['TFC_FLOW_RTE_ELEMENT'] = new Array();
	
		EntityDiagrams['TFC_FLOW_RTE_ELEMENT_LVL'] = new Array();
	
		EntityDiagrams['TIMESHEET'] = new Array();
	
		EntityDiagrams['TIMETABLE'] = new Array();
	
		EntityDiagrams['TLOF'] = new Array();
	
		EntityDiagrams['TLOF_LGT_SYS'] = new Array();
	
		EntityDiagrams['TLOF_SAFE_AREA'] = new Array();
	
		EntityDiagrams['TWY'] = new Array();
	
		EntityDiagrams['TWY_CLINE_POINT'] = new Array();
	
		EntityDiagrams['TWY_HOLDING_POSITION'] = new Array();
	
		EntityDiagrams['TWY_INTERSECTION'] = new Array();
	
		EntityDiagrams['TWY_LGT_SYS'] = new Array();
	
		EntityDiagrams['UNIT'] = new Array();
	
		EntityDiagrams['UNIT_ASSOC'] = new Array();
	
		EntityDiagrams['VOR'] = new Array();
	
		EntityDiagrams['VOR_USAGE_LIMIT'] = new Array();
	
		EntityDiagrams['[AIRSPACE_ASSOC]'] = new Array();
	
		EntityDiagrams['[DME_LIMITATION]'] = new Array();
	
		EntityDiagrams['[NDB_LIMITATION]'] = new Array();
	
		EntityDiagrams['[TACAN_LIMITATION]'] = new Array();
	
		EntityDiagrams['[VOR_LIMITATION]'] = new Array();
	
	EntityDiagrams['OXYGEN']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['NITROGEN']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['ORG_AUTH']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['TIMESHEET']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['TIMETABLE']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['PASSENGER_FACILITY']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['AD_HP_GND_SER']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['FUEL']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['OIL']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['ADDRESS']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['AD_HP']['001_AD_HP'] = '001_AD_HP';

	EntityDiagrams['FLIGHT_CLASS']['002_AD_HP_USAGE'] = '002_AD_HP_USAGE';

	EntityDiagrams['AIRCRAFT_CLASS']['002_AD_HP_USAGE'] = '002_AD_HP_USAGE';

	EntityDiagrams['AD_HP_USAGE_CONDITION']['002_AD_HP_USAGE'] = '002_AD_HP_USAGE';

	EntityDiagrams['TIMETABLE']['002_AD_HP_USAGE'] = '002_AD_HP_USAGE';

	EntityDiagrams['AD_HP_USAGE_LIMITATION']['002_AD_HP_USAGE'] = '002_AD_HP_USAGE';

	EntityDiagrams['AD_HP_USAGE']['002_AD_HP_USAGE'] = '002_AD_HP_USAGE';

	EntityDiagrams['AD_HP']['002_AD_HP_USAGE'] = '002_AD_HP_USAGE';

	EntityDiagrams['RWY_DIRECTION']['004_RWY'] = '004_RWY';

	EntityDiagrams['RWY_CLINE_POINT']['004_RWY'] = '004_RWY';

	EntityDiagrams['RWY']['004_RWY'] = '004_RWY';

	EntityDiagrams['AD_HP']['004_RWY'] = '004_RWY';

	EntityDiagrams['LANDING_PROTECTION_AREA']['005_RWY_DIRECTION'] = '005_RWY_DIRECTION';

	EntityDiagrams['RWY_PROTECT_AREA']['005_RWY_DIRECTION'] = '005_RWY_DIRECTION';

	EntityDiagrams['AD_HP_SURFACE_VERTEX']['005_RWY_DIRECTION'] = '005_RWY_DIRECTION';

	EntityDiagrams['AD_HP_SURFACE_BORDER']['005_RWY_DIRECTION'] = '005_RWY_DIRECTION';

	EntityDiagrams['SWY']['005_RWY_DIRECTION'] = '005_RWY_DIRECTION';

	EntityDiagrams['RWY_DIRECTION']['005_RWY_DIRECTION'] = '005_RWY_DIRECTION';

	EntityDiagrams['RWY']['005_RWY_DIRECTION'] = '005_RWY_DIRECTION';

	EntityDiagrams['TWY']['006_RWY_DIRECTION_DE'] = '006_RWY_DIRECTION_DECL_DIST';

	EntityDiagrams['RWY_DIRECTION_DECL_DIST']['006_RWY_DIRECTION_DE'] = '006_RWY_DIRECTION_DECL_DIST';

	EntityDiagrams['RWY_DIRECTION']['006_RWY_DIRECTION_DE'] = '006_RWY_DIRECTION_DECL_DIST';

	EntityDiagrams['SURFACE_LGT_SYS']['007_RWY_DIRECTION_LG'] = '007_RWY_DIRECTION_LGT';

	EntityDiagrams['RWY_DIRECTION_LGT_SYS']['007_RWY_DIRECTION_LG'] = '007_RWY_DIRECTION_LGT';

	EntityDiagrams['RWY_DIRECTION_ALS']['007_RWY_DIRECTION_LG'] = '007_RWY_DIRECTION_LGT';

	EntityDiagrams['RWY_DIRECTION']['007_RWY_DIRECTION_LG'] = '007_RWY_DIRECTION_LGT';

	EntityDiagrams['AD_HP_SURFACE_VERTEX']['008_TLOF_GEOMETRY'] = '008_TLOF_GEOMETRY';

	EntityDiagrams['AD_HP_SURFACE_BORDER']['008_TLOF_GEOMETRY'] = '008_TLOF_GEOMETRY';

	EntityDiagrams['TLOF_LGT_SYS']['008_TLOF_GEOMETRY'] = '008_TLOF_GEOMETRY';

	EntityDiagrams['TLOF']['008_TLOF_GEOMETRY'] = '008_TLOF_GEOMETRY';

	EntityDiagrams['AD_HP']['008_TLOF_GEOMETRY'] = '008_TLOF_GEOMETRY';

	EntityDiagrams['AD_HP']['009_TLOF_FATO'] = '009_TLOF_FATO';

	EntityDiagrams['TLOF']['009_TLOF_FATO'] = '009_TLOF_FATO';

	EntityDiagrams['FATO_DIRECTION']['009_TLOF_FATO'] = '009_TLOF_FATO';

	EntityDiagrams['FATO_CLINE_POINT']['009_TLOF_FATO'] = '009_TLOF_FATO';

	EntityDiagrams['FATO']['009_TLOF_FATO'] = '009_TLOF_FATO';

	EntityDiagrams['LANDING_PROTECTION_AREA']['010_FATO_PROTECT_ARE'] = '010_FATO_PROTECT_AREA';

	EntityDiagrams['FATO_PROTECT_AREA']['010_FATO_PROTECT_ARE'] = '010_FATO_PROTECT_AREA';

	EntityDiagrams['AD_HP_SURFACE_BORDER']['010_FATO_PROTECT_ARE'] = '010_FATO_PROTECT_AREA';

	EntityDiagrams['AD_HP_SURFACE_VERTEX']['010_FATO_PROTECT_ARE'] = '010_FATO_PROTECT_AREA';

	EntityDiagrams['FATO_DIRECTION']['010_FATO_PROTECT_ARE'] = '010_FATO_PROTECT_AREA';

	EntityDiagrams['SURFACE_LGT_GROUP']['011_FATO_DIRECTION_L'] = '011_FATO_DIRECTION_LGT';

	EntityDiagrams['SURFACE_LGT_SYS']['011_FATO_DIRECTION_L'] = '011_FATO_DIRECTION_LGT';

	EntityDiagrams['FATO_DIRECTION_LGT_SYS']['011_FATO_DIRECTION_L'] = '011_FATO_DIRECTION_LGT';

	EntityDiagrams['FATO_DIRECTION_ALS']['011_FATO_DIRECTION_L'] = '011_FATO_DIRECTION_LGT';

	EntityDiagrams['FATO_DIRECTION']['011_FATO_DIRECTION_L'] = '011_FATO_DIRECTION_LGT';

	EntityDiagrams['TWY']['012_FATO_DIRECTION_D'] = '012_FATO_DIRECTION_DECL_DIST';

	EntityDiagrams['FATO_DIRECTION_DECL_DIST']['012_FATO_DIRECTION_D'] = '012_FATO_DIRECTION_DECL_DIST';

	EntityDiagrams['FATO_DIRECTION']['012_FATO_DIRECTION_D'] = '012_FATO_DIRECTION_DECL_DIST';

	EntityDiagrams['LANDING_PROTECTION_AREA']['013_TLOF_SAFE_AREA'] = '013_TLOF_SAFE_AREA';

	EntityDiagrams['TLOF_SAFE_AREA']['013_TLOF_SAFE_AREA'] = '013_TLOF_SAFE_AREA';

	EntityDiagrams['AD_HP_SURFACE_VERTEX']['013_TLOF_SAFE_AREA'] = '013_TLOF_SAFE_AREA';

	EntityDiagrams['AD_HP_SURFACE_BORDER']['013_TLOF_SAFE_AREA'] = '013_TLOF_SAFE_AREA';

	EntityDiagrams['TLOF']['013_TLOF_SAFE_AREA'] = '013_TLOF_SAFE_AREA';

	EntityDiagrams['SURFACE_LGT_SYS']['014_TWY'] = '014_TWY';

	EntityDiagrams['TWY_LGT_SYS']['014_TWY'] = '014_TWY';

	EntityDiagrams['TWY_HOLDING_POSITION']['014_TWY'] = '014_TWY';

	EntityDiagrams['TWY_CLINE_POINT']['014_TWY'] = '014_TWY';

	EntityDiagrams['TWY']['014_TWY'] = '014_TWY';

	EntityDiagrams['AD_HP']['014_TWY'] = '014_TWY';

	EntityDiagrams['AD_HP_SURFACE_VERTEX']['015_APRON'] = '015_APRON';

	EntityDiagrams['AD_HP_SURFACE_BORDER']['015_APRON'] = '015_APRON';

	EntityDiagrams['APRON']['015_APRON'] = '015_APRON';

	EntityDiagrams['AD_HP']['015_APRON'] = '015_APRON';

	EntityDiagrams['SURFACE_LGT_SYS']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['APRON_LGT_SYS']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['APRON']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['ANGLE_INDICATION']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['TWY_HOLDING_POSITION']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['NAV_SYS_CHECKPOINT']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['GATE_STAND']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['DISTANCE_INDICATION']['016_APRON_GATE_STAND'] = '016_APRON_GATE_STAND';

	EntityDiagrams['LANDING_PROTECTION_AREA']['017_AD_HP_SURFACES'] = '017_AD_HP_SURFACES';

	EntityDiagrams['TLOF_SAFE_AREA']['017_AD_HP_SURFACES'] = '017_AD_HP_SURFACES';

	EntityDiagrams['RWY_PROTECT_AREA']['017_AD_HP_SURFACES'] = '017_AD_HP_SURFACES';

	EntityDiagrams['FATO_PROTECT_AREA']['017_AD_HP_SURFACES'] = '017_AD_HP_SURFACES';

	EntityDiagrams['TLOF']['017_AD_HP_SURFACES'] = '017_AD_HP_SURFACES';

	EntityDiagrams['RWY_DIRECTION']['017_AD_HP_SURFACES'] = '017_AD_HP_SURFACES';

	EntityDiagrams['FATO_DIRECTION']['017_AD_HP_SURFACES'] = '017_AD_HP_SURFACES';

	EntityDiagrams['TWY']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['TLOF']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['SWY']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['SURFACE_CHARACTERISTICS']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['RWY']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['LANDING_PROTECTION_AREA']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['FATO']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['APRON']['018_SURFACE_CHARACTE'] = '018_SURFACE_CHARACTERISTICS';

	EntityDiagrams['SURFACE_LGT_GROUP']['019_SURFACE_LIGHT_SY'] = '019_SURFACE_LIGHT_SYSTEM';

	EntityDiagrams['SURFACE_LGT_SYS']['019_SURFACE_LIGHT_SY'] = '019_SURFACE_LIGHT_SYSTEM';

	EntityDiagrams['APRON_LGT_SYS']['019_SURFACE_LIGHT_SY'] = '019_SURFACE_LIGHT_SYSTEM';

	EntityDiagrams['TLOF_LGT_SYS']['019_SURFACE_LIGHT_SY'] = '019_SURFACE_LIGHT_SYSTEM';

	EntityDiagrams['RWY_DIRECTION_LGT_SYS']['019_SURFACE_LIGHT_SY'] = '019_SURFACE_LIGHT_SYSTEM';

	EntityDiagrams['FATO_DIRECTION_LGT_SYS']['019_SURFACE_LIGHT_SY'] = '019_SURFACE_LIGHT_SYSTEM';

	EntityDiagrams['TWY_LGT_SYS']['019_SURFACE_LIGHT_SY'] = '019_SURFACE_LIGHT_SYSTEM';

	EntityDiagrams['TIMETABLE']['020_AERO_GND_LGT'] = '020_AERO_GND_LGT';

	EntityDiagrams['TIMESHEET']['020_AERO_GND_LGT'] = '020_AERO_GND_LGT';

	EntityDiagrams['AERO_GND_LGT']['020_AERO_GND_LGT'] = '020_AERO_GND_LGT';

	EntityDiagrams['AD_HP']['020_AERO_GND_LGT'] = '020_AERO_GND_LGT';

	EntityDiagrams['TIMETABLE']['021_ILS'] = '021_ILS';

	EntityDiagrams['RWY_DIRECTION']['021_ILS'] = '021_ILS';

	EntityDiagrams['MKR']['021_ILS'] = '021_ILS';

	EntityDiagrams['ILS_LLZ']['021_ILS'] = '021_ILS';

	EntityDiagrams['ILS_GP']['021_ILS'] = '021_ILS';

	EntityDiagrams['ILS']['021_ILS'] = '021_ILS';

	EntityDiagrams['FATO_DIRECTION']['021_ILS'] = '021_ILS';

	EntityDiagrams['DME']['021_ILS'] = '021_ILS';

	EntityDiagrams['TIMETABLE']['022_MLS'] = '022_MLS';

	EntityDiagrams['RWY_DIRECTION']['022_MLS'] = '022_MLS';

	EntityDiagrams['MLS_ELEVATION']['022_MLS'] = '022_MLS';

	EntityDiagrams['MLS_AZIMUTH']['022_MLS'] = '022_MLS';

	EntityDiagrams['MLS']['022_MLS'] = '022_MLS';

	EntityDiagrams['FATO_DIRECTION']['022_MLS'] = '022_MLS';

	EntityDiagrams['DME']['022_MLS'] = '022_MLS';

	EntityDiagrams['FATO_DIRECTION']['023_OBSTACLE'] = '023_OBSTACLE';

	EntityDiagrams['RWY_DIRECTION']['023_OBSTACLE'] = '023_OBSTACLE';

	EntityDiagrams['RWY_DIRECTION_OBSTACLE']['023_OBSTACLE'] = '023_OBSTACLE';

	EntityDiagrams['OBSTACLE']['023_OBSTACLE'] = '023_OBSTACLE';

	EntityDiagrams['FATO_DIRECTION_OBSTACLE']['023_OBSTACLE'] = '023_OBSTACLE';

	EntityDiagrams['AD_HP_OBSTACLE']['023_OBSTACLE'] = '023_OBSTACLE';

	EntityDiagrams['AD_HP']['023_OBSTACLE'] = '023_OBSTACLE';

	EntityDiagrams['TWY']['024_TWY_INTERSECTION'] = '024_TWY_INTERSECTION';

	EntityDiagrams['TWY_INTERSECTION']['024_TWY_INTERSECTION'] = '024_TWY_INTERSECTION';

	EntityDiagrams['RWY_DIRECTION']['024_TWY_INTERSECTION'] = '024_TWY_INTERSECTION';

	EntityDiagrams['FATO_DIRECTION']['024_TWY_INTERSECTION'] = '024_TWY_INTERSECTION';

	EntityDiagrams['DME_USAGE_LIMIT']['025_DME'] = '025_DME';

	EntityDiagrams['VOR']['025_DME'] = '025_DME';

	EntityDiagrams['TIMETABLE']['025_DME'] = '025_DME';

	EntityDiagrams['TIMESHEET']['025_DME'] = '025_DME';

	EntityDiagrams['ORG_AUTH']['025_DME'] = '025_DME';

	EntityDiagrams['DME']['025_DME'] = '025_DME';

	EntityDiagrams['NDB_USAGE_LIMIT']['026_NDB'] = '026_NDB';

	EntityDiagrams['NDB']['026_NDB'] = '026_NDB';

	EntityDiagrams['MKR']['026_NDB'] = '026_NDB';

	EntityDiagrams['TIMETABLE']['026_NDB'] = '026_NDB';

	EntityDiagrams['TIMESHEET']['026_NDB'] = '026_NDB';

	EntityDiagrams['ORG_AUTH']['026_NDB'] = '026_NDB';

	EntityDiagrams['NDB']['027_MKR'] = '027_MKR';

	EntityDiagrams['MKR']['027_MKR'] = '027_MKR';

	EntityDiagrams['ILS']['027_MKR'] = '027_MKR';

	EntityDiagrams['TIMETABLE']['027_MKR'] = '027_MKR';

	EntityDiagrams['TIMESHEET']['027_MKR'] = '027_MKR';

	EntityDiagrams['ORG_AUTH']['027_MKR'] = '027_MKR';

	EntityDiagrams['TACAN_USAGE_LIMIT']['028_TACAN'] = '028_TACAN';

	EntityDiagrams['VOR']['028_TACAN'] = '028_TACAN';

	EntityDiagrams['TACAN']['028_TACAN'] = '028_TACAN';

	EntityDiagrams['TIMETABLE']['028_TACAN'] = '028_TACAN';

	EntityDiagrams['TIMESHEET']['028_TACAN'] = '028_TACAN';

	EntityDiagrams['ORG_AUTH']['028_TACAN'] = '028_TACAN';

	EntityDiagrams['VOR_USAGE_LIMIT']['029_VOR'] = '029_VOR';

	EntityDiagrams['VOR']['029_VOR'] = '029_VOR';

	EntityDiagrams['TIMETABLE']['029_VOR'] = '029_VOR';

	EntityDiagrams['TACAN']['029_VOR'] = '029_VOR';

	EntityDiagrams['ORG_AUTH']['029_VOR'] = '029_VOR';

	EntityDiagrams['DME']['029_VOR'] = '029_VOR';

	EntityDiagrams['VOR']['030_AD_HP_NAV_AID'] = '030_AD_HP_NAVAID';

	EntityDiagrams['TACAN']['030_AD_HP_NAV_AID'] = '030_AD_HP_NAVAID';

	EntityDiagrams['SIGNIFICANT_POINT']['030_AD_HP_NAV_AID'] = '030_AD_HP_NAVAID';

	EntityDiagrams['NDB']['030_AD_HP_NAV_AID'] = '030_AD_HP_NAVAID';

	EntityDiagrams['MKR']['030_AD_HP_NAV_AID'] = '030_AD_HP_NAVAID';

	EntityDiagrams['DME']['030_AD_HP_NAV_AID'] = '030_AD_HP_NAVAID';

	EntityDiagrams['TACAN_USAGE_LIMIT']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['NDB_USAGE_LIMIT']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['DME_USAGE_LIMIT']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['VOR_USAGE_LIMIT']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['VOR']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['TACAN']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['NDB']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['NAVAID_LIMITATION']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['DME']['031_NAVAID_LIMITATIO'] = '031_NAVAID_LIMITATION';

	EntityDiagrams['TLOF']['032_DESIGNATED_POINT'] = '032_DESIGNATED_POINT';

	EntityDiagrams['RWY_CLINE_POINT']['032_DESIGNATED_POINT'] = '032_DESIGNATED_POINT';

	EntityDiagrams['FATO_CLINE_POINT']['032_DESIGNATED_POINT'] = '032_DESIGNATED_POINT';

	EntityDiagrams['DESIGNATED_POINT']['032_DESIGNATED_POINT'] = '032_DESIGNATED_POINT';

	EntityDiagrams['AD_HP']['032_DESIGNATED_POINT'] = '032_DESIGNATED_POINT';

	EntityDiagrams['VOR']['033_SIGNIFICANT_POIN'] = '033_SIGNIFICANT_POIN';

	EntityDiagrams['TACAN']['033_SIGNIFICANT_POIN'] = '033_SIGNIFICANT_POIN';

	EntityDiagrams['SIGNIFICANT_POINT']['033_SIGNIFICANT_POIN'] = '033_SIGNIFICANT_POIN';

	EntityDiagrams['NDB']['033_SIGNIFICANT_POIN'] = '033_SIGNIFICANT_POIN';

	EntityDiagrams['MKR']['033_SIGNIFICANT_POIN'] = '033_SIGNIFICANT_POIN';

	EntityDiagrams['DME']['033_SIGNIFICANT_POIN'] = '033_SIGNIFICANT_POIN';

	EntityDiagrams['DESIGNATED_POINT']['033_SIGNIFICANT_POIN'] = '033_SIGNIFICANT_POIN';

	EntityDiagrams['TWY_HOLDING_POSITION']['034_ANGLE_INDICATION'] = '034_ANGLE_INDICATION';

	EntityDiagrams['GATE_STAND']['034_ANGLE_INDICATION'] = '034_ANGLE_INDICATION';

	EntityDiagrams['VOR']['034_ANGLE_INDICATION'] = '034_ANGLE_INDICATION';

	EntityDiagrams['TACAN']['034_ANGLE_INDICATION'] = '034_ANGLE_INDICATION';

	EntityDiagrams['SIGNIFICANT_POINT']['034_ANGLE_INDICATION'] = '034_ANGLE_INDICATION';

	EntityDiagrams['NDB']['034_ANGLE_INDICATION'] = '034_ANGLE_INDICATION';

	EntityDiagrams['NAV_SYS_CHECKPOINT']['034_ANGLE_INDICATION'] = '034_ANGLE_INDICATION';

	EntityDiagrams['DME']['035_DISTANCE_INDICAT'] = '035_DISTANCE_INDICATION';

	EntityDiagrams['DISTANCE_INDICATION']['035_DISTANCE_INDICAT'] = '035_DISTANCE_INDICATION';

	EntityDiagrams['TWY_HOLDING_POSITION']['035_DISTANCE_INDICAT'] = '035_DISTANCE_INDICATION';

	EntityDiagrams['GATE_STAND']['035_DISTANCE_INDICAT'] = '035_DISTANCE_INDICATION';

	EntityDiagrams['TACAN']['035_DISTANCE_INDICAT'] = '035_DISTANCE_INDICATION';

	EntityDiagrams['SIGNIFICANT_POINT']['035_DISTANCE_INDICAT'] = '035_DISTANCE_INDICATION';

	EntityDiagrams['NAV_SYS_CHECKPOINT']['035_DISTANCE_INDICAT'] = '035_DISTANCE_INDICATION';

	EntityDiagrams['TIMETABLE']['036_SPEC_NAV_SYS'] = '036_SPEC_NAV_SYS';

	EntityDiagrams['TIMESHEET']['036_SPEC_NAV_SYS'] = '036_SPEC_NAV_SYS';

	EntityDiagrams['SPEC_NAV_SYS']['036_SPEC_NAV_SYS'] = '036_SPEC_NAV_SYS';

	EntityDiagrams['SPEC_NAV_STATION']['036_SPEC_NAV_SYS'] = '036_SPEC_NAV_SYS';

	EntityDiagrams['ORG_AUTH']['036_SPEC_NAV_SYS'] = '036_SPEC_NAV_SYS';

	EntityDiagrams['SIGNIFICANT_POINT']['037_RTE_SEG'] = '037_RTE_SEG';

	EntityDiagrams['SEGMENT']['037_RTE_SEG'] = '037_RTE_SEG';

	EntityDiagrams['RTE_SEG_USE']['037_RTE_SEG'] = '037_RTE_SEG';

	EntityDiagrams['RTE_SEG']['037_RTE_SEG'] = '037_RTE_SEG';

	EntityDiagrams['EN_ROUTE_RTE']['037_RTE_SEG'] = '037_RTE_SEG';

	EntityDiagrams['TIMETABLE']['038_RTE_SEG_USE'] = '038_RTE_SEG_USE';

	EntityDiagrams['TIMESHEET']['038_RTE_SEG_USE'] = '038_RTE_SEG_USE';

	EntityDiagrams['RTE_SEG_USE_LVL']['038_RTE_SEG_USE'] = '038_RTE_SEG_USE';

	EntityDiagrams['RTE_SEG_USE']['038_RTE_SEG_USE'] = '038_RTE_SEG_USE';

	EntityDiagrams['RTE_SEG']['038_RTE_SEG_USE'] = '038_RTE_SEG_USE';

	EntityDiagrams['PREDEFINED_LVL_COLUMN']['038_RTE_SEG_USE'] = '038_RTE_SEG_USE';

	EntityDiagrams['AIRSPACE_CORRIDOR']['039_AIRSPACE'] = '039_AIRSPACE';

	EntityDiagrams['AIRSPACE_AGGREG_COMP']['039_AIRSPACE'] = '039_AIRSPACE';

	EntityDiagrams['AIRSPACE_DERIV_GEOMETRY']['039_AIRSPACE'] = '039_AIRSPACE';

	EntityDiagrams['AIRSPACE_BORDER']['039_AIRSPACE'] = '039_AIRSPACE';

	EntityDiagrams['TIMETABLE']['039_AIRSPACE'] = '039_AIRSPACE';

	EntityDiagrams['RTE_SEG']['039_AIRSPACE'] = '039_AIRSPACE';

	EntityDiagrams['AIRSPACE']['039_AIRSPACE'] = '039_AIRSPACE';

	EntityDiagrams['SIGNIFICANT_POINT']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['AIRSPACE_CIRCLE_VERTEX']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['AIRSPACE_CORRIDOR']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['GEO_BORDER_VERTEX']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['GEO_BORDER']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['AIRSPACE_VERTEX']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['AIRSPACE_CLINE_VERTEX']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['AIRSPACE_BORDER_VERTEX']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['AIRSPACE_BORDER']['040_AIRSPACE_VERTEX'] = '040_AIRSPACE_VERTEX';

	EntityDiagrams['AIRSPACE_ASSOCIATION']['041_AIRSPACE_ASSOC'] = '041_AIRSPACE_ASSOC';

	EntityDiagrams['AIRSPACE']['041_AIRSPACE_ASSOC'] = '041_AIRSPACE_ASSOC';

	EntityDiagrams['FATO_DIRECTION']['042_SID'] = '042_SID';

	EntityDiagrams['SID']['042_SID'] = '042_SID';

	EntityDiagrams['RWY_DIRECTION']['042_SID'] = '042_SID';

	EntityDiagrams['PROCEDURE_LEG']['042_SID'] = '042_SID';

	EntityDiagrams['MSA_GROUP']['042_SID'] = '042_SID';

	EntityDiagrams['AD_HP']['042_SID'] = '042_SID';

	EntityDiagrams['SID_USAGE']['043_SID_USAGE'] = '043_SID_USAGE';

	EntityDiagrams['SID']['043_SID_USAGE'] = '043_SID_USAGE';

	EntityDiagrams['TIMETABLE']['043_SID_USAGE'] = '043_SID_USAGE';

	EntityDiagrams['TIMESHEET']['043_SID_USAGE'] = '043_SID_USAGE';

	EntityDiagrams['FATO_DIRECTION']['044_STAR'] = '044_STAR';

	EntityDiagrams['FATO_DIRECTION_STAR']['044_STAR'] = '044_STAR';

	EntityDiagrams['STAR']['044_STAR'] = '044_STAR';

	EntityDiagrams['RWY_DIRECTION_STAR']['044_STAR'] = '044_STAR';

	EntityDiagrams['RWY_DIRECTION']['044_STAR'] = '044_STAR';

	EntityDiagrams['PROCEDURE_LEG']['044_STAR'] = '044_STAR';

	EntityDiagrams['MSA_GROUP']['044_STAR'] = '044_STAR';

	EntityDiagrams['AD_HP']['044_STAR'] = '044_STAR';

	EntityDiagrams['STAR_USAGE']['045_STAR_USAGE'] = '045_STAR_USAGE';

	EntityDiagrams['STAR']['045_STAR_USAGE'] = '045_STAR_USAGE';

	EntityDiagrams['TIMETABLE']['045_STAR_USAGE'] = '045_STAR_USAGE';

	EntityDiagrams['TIMESHEET']['045_STAR_USAGE'] = '045_STAR_USAGE';

	EntityDiagrams['PROCEDURE_LEG']['046_IAP'] = '046_IAP';

	EntityDiagrams['MSA_GROUP']['046_IAP'] = '046_IAP';

	EntityDiagrams['TIMETABLE']['046_IAP'] = '046_IAP';

	EntityDiagrams['TIMESHEET']['046_IAP'] = '046_IAP';

	EntityDiagrams['OCA_OCH']['046_IAP'] = '046_IAP';

	EntityDiagrams['IAP_USAGE']['046_IAP'] = '046_IAP';

	EntityDiagrams['IAP']['046_IAP'] = '046_IAP';

	EntityDiagrams['STAR']['047_PROCEDURE_LEG'] = '047_PROCEDURE_LEG';

	EntityDiagrams['SID']['047_PROCEDURE_LEG'] = '047_PROCEDURE_LEG';

	EntityDiagrams['PROCEDURE_LEG']['047_PROCEDURE_LEG'] = '047_PROCEDURE_LEG';

	EntityDiagrams['IAP']['047_PROCEDURE_LEG'] = '047_PROCEDURE_LEG';

	EntityDiagrams['HOLDING_PROCEDURE']['047_PROCEDURE_LEG'] = '047_PROCEDURE_LEG';

	EntityDiagrams['SIGNIFICANT_POINT']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['STAR']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['SID']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['VOR']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['NDB']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['IAP']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['MSA_GROUP']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['MSA']['048_MSA_GROUP'] = '048_MSA_GROUP';

	EntityDiagrams['UNIT']['049_ORG_AUTH'] = '049_ORG_AUTH';

	EntityDiagrams['ORG_AUTH_ASSOC']['049_ORG_AUTH'] = '049_ORG_AUTH';

	EntityDiagrams['ORG_AUTH']['049_ORG_AUTH'] = '049_ORG_AUTH';

	EntityDiagrams['SERVICE']['050_UNIT'] = '050_UNIT';

	EntityDiagrams['UNIT_ASSOC']['050_UNIT'] = '050_UNIT';

	EntityDiagrams['UNIT']['050_UNIT'] = '050_UNIT';

	EntityDiagrams['TIMETABLE']['050_UNIT'] = '050_UNIT';

	EntityDiagrams['ORG_AUTH']['050_UNIT'] = '050_UNIT';

	EntityDiagrams['ORG_AUTH']['051_AUTH_FOR_AIRSPAC'] = '051_AUTHORITY_FOR_AIRSPACE';

	EntityDiagrams['AUTH_FOR_AIRSPACE']['051_AUTH_FOR_AIRSPAC'] = '051_AUTHORITY_FOR_AIRSPACE';

	EntityDiagrams['AIRSPACE']['051_AUTH_FOR_AIRSPAC'] = '051_AUTHORITY_FOR_AIRSPACE';

	EntityDiagrams['ORG_AUTH']['052_SERVICE'] = '052_SERVICE';

	EntityDiagrams['TIMESHEET']['052_SERVICE'] = '052_SERVICE';

	EntityDiagrams['UNIT']['052_SERVICE'] = '052_SERVICE';

	EntityDiagrams['TIMETABLE']['052_SERVICE'] = '052_SERVICE';

	EntityDiagrams['SERVICE']['052_SERVICE'] = '052_SERVICE';

	EntityDiagrams['FREQUENCY']['052_SERVICE'] = '052_SERVICE';

	EntityDiagrams['CALLSIGN_DETAIL']['052_SERVICE'] = '052_SERVICE';

	EntityDiagrams['HOLDING_PROCEDURE']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['RTE_SEG']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['IAP']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['STAR']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SID']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE_ON_STAR']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE_ON_SID']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE_ON_RTE_SEG']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE_ON_IAP']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE_ON_HOLDING_PROC']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE_IN_AIRSPACE']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE_AT_AD_HP']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SERVICE']['053_SERVICE_LOCATION'] = '053_SERVICE_LOCATION';

	EntityDiagrams['SIGNIFICANT_POINT_IN_AS']['054_SIGNIFICANT_POIN'] = '054_SIGNIFICANT_POINT';

	EntityDiagrams['SIGNIFICANT_POINT']['054_SIGNIFICANT_POIN'] = '054_SIGNIFICANT_POINT';

	EntityDiagrams['AIRSPACE']['054_SIGNIFICANT_POIN'] = '054_SIGNIFICANT_POINT';

	EntityDiagrams['SPECIAL_DATE']['055_SPECIAL_DATE'] = '055_SPECIAL_DATE';

	EntityDiagrams['ORG_AUTH']['055_SPECIAL_DATE'] = '055_SPECIAL_DATE';

	EntityDiagrams['TIMESHEET']['056_TFC_FLOW_RESTR'] = '056_TFC_FLOW_RESTR';

	EntityDiagrams['TIMETABLE']['056_TFC_FLOW_RESTR'] = '056_TFC_FLOW_RESTR';

	EntityDiagrams['TFC_FLOW_RTE']['056_TFC_FLOW_RESTR'] = '056_TFC_FLOW_RESTR';

	EntityDiagrams['TFC_FLOW_RESTR']['056_TFC_FLOW_RESTR'] = '056_TFC_FLOW_RESTR';

	EntityDiagrams['FLOW_COND_COMBINATION']['056_TFC_FLOW_RESTR'] = '056_TFC_FLOW_RESTR';

	EntityDiagrams['STAR']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['SID']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['SEGMENT']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['TFC_FLOW_RESTR']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['SIGNIFICANT_POINT']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['RTE_PORTION']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['ORG_AUTH']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['FLOW_COND_ELEMENT_LVL']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['FLOW_COND_ELEMENT']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['FLOW_COND_COMBINATION']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['FLIGHT_CLASS']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['DIRECT_FLIGHT']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['DIRECT_FLIGHT_CLASS']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['DIRECT_FLIGHT_SEGMENT']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['AIRSPACE_BORDER_CROSSING']['057_FLOW_COND_COMBIN'] = '057_FLOW_COND_COMBINATION';

	EntityDiagrams['DIRECT_FLIGHT']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['DIRECT_FLIGHT_CLASS']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['DIRECT_FLIGHT_SEGMENT']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['SEGMENT']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['TFC_FLOW_RESTR']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT_LVL']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['TFC_FLOW_RTE']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['STAR']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['SIGNIFICANT_POINT']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['SID']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['RTE_PORTION']['058_TFC_FLOW_RTE'] = '058_TFC_FLOW_RTE';

	EntityDiagrams['SIGNIFICANT_POINT']['059_RTE_PORTION'] = '059_RTE_PORTION';

	EntityDiagrams['SEGMENT']['059_RTE_PORTION'] = '059_RTE_PORTION';

	EntityDiagrams['RTE_SEG']['059_RTE_PORTION'] = '059_RTE_PORTION';

	EntityDiagrams['RTE_PORTION']['059_RTE_PORTION'] = '059_RTE_PORTION';

	EntityDiagrams['EN_ROUTE_RTE']['059_RTE_PORTION'] = '059_RTE_PORTION';

	EntityDiagrams['RTE_SEG_USE_LVL']['060_PREDEFINED_LVL'] = '060_PREDEFINED_LVL';

	EntityDiagrams['PREDEFINED_LVL_TABLE']['060_PREDEFINED_LVL'] = '060_PREDEFINED_LVL';

	EntityDiagrams['PREDEFINED_LVL_COLUMN']['060_PREDEFINED_LVL'] = '060_PREDEFINED_LVL';

	EntityDiagrams['PREDEFINED_LVL']['060_PREDEFINED_LVL'] = '060_PREDEFINED_LVL';

	EntityDiagrams['UNIT']['061_ADDRESS'] = '061_ADDRESS';

	EntityDiagrams['ORG_AUTH']['061_ADDRESS'] = '061_ADDRESS';

	EntityDiagrams['AIRSPACE']['061_ADDRESS'] = '061_ADDRESS';

	EntityDiagrams['AD_HP_GND_SER']['061_ADDRESS'] = '061_ADDRESS';

	EntityDiagrams['AD_HP']['061_ADDRESS'] = '061_ADDRESS';

	EntityDiagrams['ADDRESS']['061_ADDRESS'] = '061_ADDRESS';

	EntityDiagrams['TIMETABLE']['062_TIMETABLE'] = '062_TIMETABLE';

	EntityDiagrams['TIMESHEET']['062_TIMETABLE'] = '062_TIMETABLE';

	EntityDiagrams['OXYGEN']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['NITROGEN']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_DIRECTION_STAR']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FLIGHT_CLASS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AIRCRAFT_CLASS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_USAGE_LIMITATION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_USAGE_CONDITION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['SIGNIFICANT_POINT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['STAR']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_USAGE']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_NAV_AID']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FLOW_COND_ELEMENT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['SERVICE_AT_AD_HP']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['UNIT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['SURFACE_LGT_SYS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['APRON_LGT_SYS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_DIRECTION_LGT_SYS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_DIRECTION_LGT_SYS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TLOF_LGT_SYS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TWY_LGT_SYS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['DESIGNATED_POINT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_DIRECTION_STAR']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['SID']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['IAP']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['MLS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['ILS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['SURFACE_LGT_GROUP']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['LANDING_PROTECTION_AREA']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TLOF_SAFE_AREA']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_PROTECT_AREA']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_PROTECT_AREA']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_SURFACE_BORDER']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['ORG_AUTH']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TIMETABLE']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['ANGLE_INDICATION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['DISTANCE_INDICATION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_DIRECTION_OBSTACLE']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_DIRECTION_DECL_DIST']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_DIRECTION_ALS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_DIRECTION_OBSTACLE']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_DIRECTION_DECL_DIST']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_DIRECTION_ALS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['PASSENGER_FACILITY']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['OIL']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FUEL']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['ADDRESS']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_COLLOCATION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_CLINE_POINT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['SWY']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY_DIRECTION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_SURFACE_VERTEX']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_DIRECTION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO_CLINE_POINT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['RWY']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TWY_INTERSECTION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TWY']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TWY_CLINE_POINT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TWY_HOLDING_POSITION']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['NAV_SYS_CHECKPOINT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['GATE_STAND']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['APRON']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['FATO']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['TLOF']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AERO_GND_LGT']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP_GND_SER']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['AD_HP']['LARGE_AERODROME'] = 'global_AERODROME/HELIPORT';

	EntityDiagrams['SEGMENT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['RTE_PORTION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['SIGNIFICANT_POINT_IN_AS']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['HOLDING_PROCEDURE']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['FLOW_COND_ELEMENT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['AD_HP_NAV_AID']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['FATO_CLINE_POINT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['RWY_CLINE_POINT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['TLOF']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['AD_HP']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['FATO_DIRECTION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['RWY_DIRECTION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['NDB_USAGE_LIMIT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['PROCEDURE_LEG']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['TIMETABLE']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['SERVICE']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['ORG_AUTH']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['MSA_GROUP']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['TACAN_USAGE_LIMIT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['DISTANCE_INDICATION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['ANGLE_INDICATION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['VOR_USAGE_LIMIT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['VOR']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['TACAN']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['NDB']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['NAV_SYS_CHECKPOINT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['NAVAID_LIMITATION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['DESIGNATED_POINT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['SIGNIFICANT_POINT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['SPEC_NAV_STATION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['SPEC_NAV_SYS']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['MKR']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['MLS_ELEVATION']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['MLS_AZIMUTH']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['MLS']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['DME_USAGE_LIMIT']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['DME']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['ILS_LLZ']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['ILS_GP']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['ILS']['LARGENAVAIDDESIGPOIN'] = 'global_NAVAIDS_POINTS';

	EntityDiagrams['SERVICE_ON_RTE_SEG']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SPEC_NAV_SYS']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SPEC_NAV_STATION']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['MKR']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['TACAN']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['VOR']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['DME']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['NDB']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_CIRCLE_VERTEX']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_CORRIDOR']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_AGGREG_COMP']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['FLOW_COND_ELEMENT']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_DERIV_GEOMETRY']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_BORDER_CROSSING']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_ASSOCIATION']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['RTE_SEG']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['ADDRESS']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['TIMETABLE']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SERVICE_ON_STAR']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SERVICE_ON_SID']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SERVICE_ON_IAP']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SERVICE_ON_HOLDING_PROC']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SERVICE_IN_AIRSPACE']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SERVICE_AT_AD_HP']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SIGNIFICANT_POINT_IN_AS']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SPECIAL_DATE']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['ORG_AUTH_ASSOC']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AUTH_FOR_AIRSPACE']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_BORDER']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['PROCEDURE_LEG']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['HOLDING_PROCEDURE']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['IAP']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SID']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['STAR']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SIGNIFICANT_POINT']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['GEO_BORDER_VERTEX']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['GEO_BORDER']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_VERTEX']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_CLINE_VERTEX']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE_BORDER_VERTEX']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AIRSPACE']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['ORG_AUTH']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['UNIT']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['UNIT_ASSOC']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['CALLSIGN_DETAIL']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['FREQUENCY']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['SERVICE']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['AD_HP']['LARGE_SERVICE_AIRSPA'] = 'global_AIRSPACE_ORGANISATION_SERVICE';

	EntityDiagrams['TIMETABLE']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['SERVICE_ON_RTE_SEG']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['DIRECT_FLIGHT']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['RTE_PORTION']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT_LVL']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['STAR']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['SID']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['TFC_FLOW_RTE']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['TFC_FLOW_RESTR']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['FLOW_COND_COMBINATION']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['FLOW_COND_ELEMENT_LVL']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['FLIGHT_CLASS']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['AIRCRAFT_CLASS']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['FLOW_COND_ELEMENT']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['AIRSPACE_BORDER_CROSSING']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['RTE_SEG_USE_LVL']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['ORG_AUTH']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['AIRSPACE']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['AD_HP']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['SIGNIFICANT_POINT']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['SEGMENT']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['EN_ROUTE_RTE']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['RTE_SEG']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['PREDEFINED_LVL']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['PREDEFINED_LVL_COLUMN']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['PREDEFINED_LVL_TABLE']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['RTE_SEG_USE']['LARGE_ROUTE'] = 'global_ROUTES';

	EntityDiagrams['SERVICE_ON_STAR']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['SERVICE_ON_SID']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['TFC_FLOW_RTE_ELEMENT']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['RWY_DIRECTION_OBSTACLE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['SERVICE_ON_IAP']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['SERVICE_ON_HOLDING_PROC']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['FATO_DIRECTION_OBSTACLE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['OBSTACLE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['AD_HP_OBSTACLE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['TIMETABLE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['RWY_DIRECTION_STAR']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['FATO_DIRECTION_STAR']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['TLOF']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['FATO_DIRECTION']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['SID']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['SID_USAGE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['OCA_OCH']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['IAP_USAGE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['IAP']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['AD_HP']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['RWY_DIRECTION']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['STAR_USAGE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['STAR']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['MSA_GROUP']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['MSA']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['HOLDING_PROCEDURE']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['VOR']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['NDB']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['DME']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['TACAN']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['SIGNIFICANT_POINT']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['MLS']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['ILS']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['SPEC_NAV_SYS']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';

	EntityDiagrams['PROCEDURE_LEG']['LARGE_PROCEDURES'] = 'global_TERMINAL_PROCEDURES';
