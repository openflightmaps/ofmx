**Hi there**

You've guessed it, I'm the friendly service desk bot of Open Flightmaps.

Thanks for your message, I've assigned it to issue %{ISSUE_ID}. We'll try to answer as soon as possible, however, since we're all volunteers, please allow for some time.

*Blue skies and tail-winds!*

---

**OFMX Helpdesk**<br>
%{ISSUE_PATH}
