require 'json'

task default: :test

VERSIONS = JSON.load_file('schema/versions.json').compact

desc "Validate OFMX schema and example files against their respecitive schemas."
task :test do
  VERSIONS.each do |name, version|
    tests_for(version).each do |test|
      print `#{test}`
      if $? != 0
        puts "\033[31mSome tests have failed\033[0m"
        exit 1
      end
    end
  end
  puts "\033[32mAll tests have passed\033[0m"
end

desc "Show the versions of current OFMX schema."
task :versions do
  VERSIONS.each { puts "#{_1}: #{_2}" }
end

private

def tests_for(version)
  [
    "xmllint --noout --schema public/xsd/XMLSchema.xsd schema/#{version}/OFMX-*.xsd",
    "xmllint --noout --schema schema/#{version}/OFMX-Snapshot.xsd examples/#{version}/*.ofmx",
    "ckmid -s examples/#{version}/*.ofmx"
  ]
end
