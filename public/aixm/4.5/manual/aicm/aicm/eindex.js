
		var EntityIndex = new Array();
		
		EntityIndex['ADDRESS'] = 1;
	
		EntityIndex['AD_HP'] = 1;
	
		EntityIndex['AD_HP_COLLOCATION'] = 1;
	
		EntityIndex['AD_HP_GND_SER'] = 1;
	
		EntityIndex['AD_HP_NAV_AID'] = 1;
	
		EntityIndex['AD_HP_OBSTACLE'] = 1;
	
		EntityIndex['AD_HP_SURFACE_BORDER'] = 1;
	
		EntityIndex['AD_HP_SURFACE_VERTEX'] = 1;
	
		EntityIndex['AD_HP_USAGE'] = 1;
	
		EntityIndex['AD_HP_USAGE_CONDITION'] = 1;
	
		EntityIndex['AD_HP_USAGE_LIMITATION'] = 2;
	
		EntityIndex['AERO_GND_LGT'] = 2;
	
		EntityIndex['AIRCRAFT_CLASS'] = 2;
	
		EntityIndex['AIRSPACE'] = 2;
	
		EntityIndex['AIRSPACE_AGGREG_COMP'] = 2;
	
		EntityIndex['AIRSPACE_ASSOCIATION'] = 2;
	
		EntityIndex['AIRSPACE_BORDER'] = 2;
	
		EntityIndex['AIRSPACE_BORDER_CROSSING'] = 2;
	
		EntityIndex['AIRSPACE_BORDER_VERTEX'] = 2;
	
		EntityIndex['AIRSPACE_CIRCLE_VERTEX'] = 2;
	
		EntityIndex['AIRSPACE_CLINE_VERTEX'] = 3;
	
		EntityIndex['AIRSPACE_CORRIDOR'] = 3;
	
		EntityIndex['AIRSPACE_DERIV_GEOMETRY'] = 3;
	
		EntityIndex['AIRSPACE_VERTEX'] = 3;
	
		EntityIndex['ANGLE_INDICATION'] = 3;
	
		EntityIndex['APRON'] = 3;
	
		EntityIndex['APRON_LGT_SYS'] = 3;
	
		EntityIndex['AUTH_FOR_AIRSPACE'] = 3;
	
		EntityIndex['CALLSIGN_DETAIL'] = 3;
	
		EntityIndex['DESIGNATED_POINT'] = 3;
	
		EntityIndex['DIRECT_FLIGHT'] = 4;
	
		EntityIndex['DIRECT_FLIGHT_CLASS'] = 4;
	
		EntityIndex['DIRECT_FLIGHT_SEGMENT'] = 4;
	
		EntityIndex['DISTANCE_INDICATION'] = 4;
	
		EntityIndex['DME'] = 4;
	
		EntityIndex['DME_USAGE_LIMIT'] = 4;
	
		EntityIndex['EN_ROUTE_RTE'] = 4;
	
		EntityIndex['FATO'] = 4;
	
		EntityIndex['FATO_CLINE_POINT'] = 4;
	
		EntityIndex['FATO_DIRECTION'] = 4;
	
		EntityIndex['FATO_DIRECTION_ALS'] = 5;
	
		EntityIndex['FATO_DIRECTION_DECL_DIST'] = 5;
	
		EntityIndex['FATO_DIRECTION_LGT_SYS'] = 5;
	
		EntityIndex['FATO_DIRECTION_OBSTACLE'] = 5;
	
		EntityIndex['FATO_DIRECTION_STAR'] = 5;
	
		EntityIndex['FATO_PROTECT_AREA'] = 5;
	
		EntityIndex['FLIGHT_CLASS'] = 5;
	
		EntityIndex['FLOW_COND_COMBINATION'] = 5;
	
		EntityIndex['FLOW_COND_ELEMENT'] = 5;
	
		EntityIndex['FLOW_COND_ELEMENT_LVL'] = 5;
	
		EntityIndex['FREQUENCY'] = 6;
	
		EntityIndex['FUEL'] = 6;
	
		EntityIndex['GATE_STAND'] = 6;
	
		EntityIndex['GEO_BORDER'] = 6;
	
		EntityIndex['GEO_BORDER_VERTEX'] = 6;
	
		EntityIndex['HOLDING_PROCEDURE'] = 6;
	
		EntityIndex['IAP'] = 6;
	
		EntityIndex['IAP_USAGE'] = 6;
	
		EntityIndex['ILS'] = 6;
	
		EntityIndex['ILS_GP'] = 6;
	
		EntityIndex['ILS_LLZ'] = 7;
	
		EntityIndex['LANDING_PROTECTION_AREA'] = 7;
	
		EntityIndex['MKR'] = 7;
	
		EntityIndex['MLS'] = 7;
	
		EntityIndex['MLS_AZIMUTH'] = 7;
	
		EntityIndex['MLS_ELEVATION'] = 7;
	
		EntityIndex['MSA'] = 7;
	
		EntityIndex['MSA_GROUP'] = 7;
	
		EntityIndex['NAVAID_LIMITATION'] = 7;
	
		EntityIndex['NAV_SYS_CHECKPOINT'] = 7;
	
		EntityIndex['NDB'] = 8;
	
		EntityIndex['NDB_USAGE_LIMIT'] = 8;
	
		EntityIndex['NITROGEN'] = 8;
	
		EntityIndex['OBSTACLE'] = 8;
	
		EntityIndex['OCA_OCH'] = 8;
	
		EntityIndex['OIL'] = 8;
	
		EntityIndex['ORG_AUTH'] = 8;
	
		EntityIndex['ORG_AUTH_ASSOC'] = 8;
	
		EntityIndex['OXYGEN'] = 8;
	
		EntityIndex['PASSENGER_FACILITY'] = 8;
	
		EntityIndex['PREDEFINED_LVL'] = 9;
	
		EntityIndex['PREDEFINED_LVL_COLUMN'] = 9;
	
		EntityIndex['PREDEFINED_LVL_TABLE'] = 9;
	
		EntityIndex['PROCEDURE_LEG'] = 9;
	
		EntityIndex['RTE_PORTION'] = 9;
	
		EntityIndex['RTE_SEG'] = 9;
	
		EntityIndex['RTE_SEG_USE'] = 9;
	
		EntityIndex['RTE_SEG_USE_LVL'] = 9;
	
		EntityIndex['RWY'] = 9;
	
		EntityIndex['RWY_CLINE_POINT'] = 9;
	
		EntityIndex['RWY_DIRECTION'] = 10;
	
		EntityIndex['RWY_DIRECTION_ALS'] = 10;
	
		EntityIndex['RWY_DIRECTION_DECL_DIST'] = 10;
	
		EntityIndex['RWY_DIRECTION_LGT_SYS'] = 10;
	
		EntityIndex['RWY_DIRECTION_OBSTACLE'] = 10;
	
		EntityIndex['RWY_DIRECTION_STAR'] = 10;
	
		EntityIndex['RWY_PROTECT_AREA'] = 10;
	
		EntityIndex['SEGMENT'] = 10;
	
		EntityIndex['SERVICE'] = 10;
	
		EntityIndex['SERVICE_AT_AD_HP'] = 10;
	
		EntityIndex['SERVICE_IN_AIRSPACE'] = 11;
	
		EntityIndex['SERVICE_ON_HOLDING_PROC'] = 11;
	
		EntityIndex['SERVICE_ON_IAP'] = 11;
	
		EntityIndex['SERVICE_ON_RTE_SEG'] = 11;
	
		EntityIndex['SERVICE_ON_SID'] = 11;
	
		EntityIndex['SERVICE_ON_STAR'] = 11;
	
		EntityIndex['SID'] = 11;
	
		EntityIndex['SID_USAGE'] = 11;
	
		EntityIndex['SIGNIFICANT_POINT'] = 11;
	
		EntityIndex['SIGNIFICANT_POINT_IN_AS'] = 11;
	
		EntityIndex['SPECIAL_DATE'] = 12;
	
		EntityIndex['SPEC_NAV_STATION'] = 12;
	
		EntityIndex['SPEC_NAV_SYS'] = 12;
	
		EntityIndex['STAR'] = 12;
	
		EntityIndex['STAR_USAGE'] = 12;
	
		EntityIndex['SURFACE_CHARACTERISTICS'] = 12;
	
		EntityIndex['SURFACE_LGT_GROUP'] = 12;
	
		EntityIndex['SURFACE_LGT_SYS'] = 12;
	
		EntityIndex['SWY'] = 12;
	
		EntityIndex['TACAN'] = 12;
	
		EntityIndex['TACAN_USAGE_LIMIT'] = 13;
	
		EntityIndex['TFC_FLOW_RESTR'] = 13;
	
		EntityIndex['TFC_FLOW_RTE'] = 13;
	
		EntityIndex['TFC_FLOW_RTE_ELEMENT'] = 13;
	
		EntityIndex['TFC_FLOW_RTE_ELEMENT_LVL'] = 13;
	
		EntityIndex['TIMESHEET'] = 13;
	
		EntityIndex['TIMETABLE'] = 13;
	
		EntityIndex['TLOF'] = 13;
	
		EntityIndex['TLOF_LGT_SYS'] = 13;
	
		EntityIndex['TLOF_SAFE_AREA'] = 13;
	
		EntityIndex['TWY'] = 14;
	
		EntityIndex['TWY_CLINE_POINT'] = 14;
	
		EntityIndex['TWY_HOLDING_POSITION'] = 14;
	
		EntityIndex['TWY_INTERSECTION'] = 14;
	
		EntityIndex['TWY_LGT_SYS'] = 14;
	
		EntityIndex['UNIT'] = 14;
	
		EntityIndex['UNIT_ASSOC'] = 14;
	
		EntityIndex['VOR'] = 14;
	
		EntityIndex['VOR_USAGE_LIMIT'] = 14;
	
		EntityIndex['[AIRSPACE_ASSOC]'] = 14;
	
		EntityIndex['[DME_LIMITATION]'] = 15;
	
		EntityIndex['[NDB_LIMITATION]'] = 15;
	
		EntityIndex['[TACAN_LIMITATION]'] = 15;
	
		EntityIndex['[VOR_LIMITATION]'] = 15;
	