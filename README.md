[![Version](https://img.shields.io/badge/dynamic/json?label=version&url=http://schema.openflightmaps.org/versions.json&query=$.stable&color=orange)](http://schema.openflightmaps.org)
[![GitLab](https://img.shields.io/gitlab/pipeline-status/openflightmaps/ofmx?branch=main&label=validation)](https://gitlab.com/openflightmaps/ofmx)

# OFMX

OFMX (Open FlightMaps eXchange) is a suite of well-defined data formats to validate and exchange aeronautical data with [open flightmaps (OFM)](https://openflightmaps.org).

* [Homepage](https://gitlab.com/openflightmaps/ofmx)
* [Documentation](https://gitlab.com/openflightmaps/ofmx/wikis)
* [Issues](https://gitlab.com/openflightmaps/ofmx/issues)
* [Boards](https://gitlab.com/openflightmaps/ofmx/boards)

## OFMX XML

OFMX XML is an [XML](https://en.wikipedia.org/wiki/XML) dialect to exchange the complete dataset, a decluttered and extended fork of AIXM 4.5 (Aeronautical Information eXchange Model). This is the primary exchange format using the file extension `.ofmx`.

### Examples

The `examples` directory contains a few OFMX XML documents such as the following `examples/0.1/label_marker.ofmx` which describes a label marker (an OFMX extension to AIXM):

```xml
<?xml version="1.0" encoding="UTF-8"?>
<OFMX-Snapshot xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.openflightmaps.org/0.1/OFMX-Snapshot.xsd" version="0.1" origin="example" namespace="00000000-0000-0000-0000-000000000000" created="2018-01-01T12:09:15+00:00" effective="2018-01-04T00:00:00+00:00">
  <Lbm>
    <LbmUid region="LJLA">
      <codeType>TEXT</codeType>
      <txtName>XPDR FREQ EDJA</txtName>
    </LbmUid>
    <txtValueLabel>TRANSPONDER CODE 1234, LISTEN FREQ 118.100</txtValueLabel>
    <geoLat>48.98444444N</geoLat>
    <geoLong>015.29111111E</geoLong>
    <codeDatum>WGE</codeDatum>
    <ZoomLevel>
      <valZoomLevel>1</valZoomLevel>
    </ZoomLevel>
    <ZoomLevel>
      <valZoomLevel>2</valZoomLevel>
    </ZoomLevel>
    <ZoomLevel>
      <valZoomLevel>3</valZoomLevel>
      <txtValueLabel>TRANSPONDER CODE REQUIRED</txtValueLabel>
      <geoLat>48.50000000N</geoLat>
      <geoLong>015.00000000E</geoLong>
    </ZoomLevel>
    <ZoomLevel>
      <valZoomLevel>4</valZoomLevel>
    </ZoomLevel>
    <ZoomLevel>
      <valZoomLevel>7</valZoomLevel>
    </ZoomLevel>
  </Lbm>
</OFMX-Snapshot>
```

Let's validate it:

```bash
xmllint --noout --schema schema/0.1/OFMX-Snapshot.xsd examples/0.1/label_marker.ofmx
```

## OFMX CSV

For the sake of simplicity, some data subsets can also be exchanged by use of [[CSV following OFMX standards|OFMX-CSV]]. These are auxiliary exchange formats using the file extension `.csv`.

### Examples

The `examples` directory contains a few OFMX CSV documents such as the following `examples/0.1/obstacle.csv` which describes a few obstacles:

```csv
codeType,txtName,codeLgt,txtDescrMarking,geoLat,geoLong,valElev,valElevAccuracy,valHgt,codeHgtAccuracy,uomDistVer,valRadius,valRadiusAccuracy,uomRadius,codeGroupId,txtGroupName,locGroupMemberId,locLinkedToGroupMemberId,codeLinkType,datetimeValidWef,datetimeValidTil,txtRmk,source
ANTENNA,DROITWICH LW NORTH,Y,Red lighting,52.29639600N,2.10675400W,56,0,214,Y,M,200,10,M,3d3b794b-590e-42dd-abad-3e943f75ab0d,Longwave antenna masts,1,,,2018-08-01T00:00:00Z,2020-07-31T24:00:00Z,Longwave antenna see https://en.wikipedia.org/wiki/Droitwich_Transmitting_Station,EG|ENR|5.4|2018-01-04|2947
ANTENNA,DROITWICH LW SOUTH,Y,Red lighting,52.29457700N,2.10568600W,56,0,214,Y,M,200,10,M,3d3b794b-590e-42dd-abad-3e943f75ab0d,Longwave antenna masts,2,1,CABLE,2018-08-01T00:00:00Z,2020-07-31T24:00:00Z,Longwave antenna see https://en.wikipedia.org/wiki/Droitwich_Transmitting_Station,EG|ENR|5.4|2018-01-04|2947
TREE,LFNT 34L A,N,,43.98977100N,4.75587900E,55,2,12,N,M,2,1,M,83d659d8-4d39-4628-be86-a2bcadafc09a,,,,,,,Close to the beginning of runway 34L,LF|ENR|5.4|2018-01-04|838
TREE,LFNT 34L B,N,,43.98970100N,4.75601200E,55,2,12,N,M,2,1,M,83d659d8-4d39-4628-be86-a2bcadafc09a,,,,,,,Close to the beginning of runway 34L,LF|ENR|5.4|2018-01-04|838
WINDTURBINE,GIRONA,Y,Red lighting and orange reflective strips,42.44166667N,2.81972222E,697,,125,Y,M,60,5,M,,,,,,2018-08-10T00:00:00Z,,Coordinates not verified,LE|ENR|5.4|2018-01-04|3847
```

To validate it, check out [CSVLint](https://csvlint.io) following [OFMX standards](https://gitlab.com/openflightmaps/ofmx/wikis/OFMX-CSV).

## Versions

OFMX uses [semantic versioning](https://gitlab.com/openflightmaps/ofmx/blob/master/README.md#versions) MAJOR.MINOR.PATCH. However, [OFMX documents reference the schema by MAJOR.MINOR](Snapshot#ofmx-snapshot). PATCH releases are guaranteed to be compatible with their corresponding MINOR version, therefore only MAJOR.MINOR is used for the schema. However, the complete MAJOR.MINOR.PATCH version is used for the Git release tag, see the [release section](#release) below.

You can query the current schema versions on [http://schema.openflightmaps.org/versions.json](http://schema.openflightmaps.org/versions.json)

## Development

### Tests

```bash
bundle install
bundle exec rake test
```

### Diffs

The [changelog](https://gitlab.com/openflightmaps/ofmx/blob/master/CHANGELOG.md) gives you an overview of all additions made to AIXM. For more details just `diff` the corresponding schema files against "base" which contains the forked and functionally unmodified AIXM 4.5 schema.

```bash
diff -burN schema_base/4.5-r2/OFMX-Snapshot.xsd schema/0.1/OFMX-Snapshot.xsd
```

### Release

Any push or merge on the `main` branch trigger validations of the schema and all examples. If everything passes, the schema files are deployed to https://schema.openflightmaps.org automatically.

To cut a formal release:

1. Make sure all the changes are documented in the wiki
2. If the changes are breaking: Increment the MAJOR or MINOR version in schema files, examples, README and wiki
3. Complete the CHANGELOG
4. Run the tests with `bundle exec rake test`
5. Push or merge on the `main` branch
6. Create a [`vMAJOR.MINOR.PATCH` Git release tag with release notes](https://gitlab.com/openflightmaps/ofmx/-/tags/new)

### Contribute

Found a bug or problem? Please [send an email to the OFMX helpdesk](mailto:ofmx-helpdesk@openflightmaps.org) or [submit an issue on GitLab](https://gitlab.com/openflightmaps/ofmx/issues).

To contribute code, [fork the project on GitLab](https://docs.gitlab.com/ce/user/project/repository/forking_workflow.html), add your code and submit a merge request.

## References

* [open flightmaps](https://openflightmaps.org)
* [AIXM](http://aixm.aero)
* [AIXM 4.5](http://aixm.aero/document/aixm-45-specification)
