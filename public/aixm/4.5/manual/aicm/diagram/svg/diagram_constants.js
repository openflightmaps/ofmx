 
function DDim(width,height) {
	this.width = width;
	this.height = height;
}
var svgdims = new Array();
	
	
svgdims['001_AD_HP'] = new DDim(842,595);

	
svgdims['002_AD_HP_USAGE'] = new DDim(842,595);

	
svgdims['003_AD_HP_COLLOCATIO'] = new DDim(842,595);

	
svgdims['004_RWY'] = new DDim(842,595);

	
svgdims['005_RWY_DIRECTION'] = new DDim(842,595);

	
svgdims['006_RWY_DIRECTION_DE'] = new DDim(842,595);

	
svgdims['007_RWY_DIRECTION_LG'] = new DDim(842,595);

	
svgdims['008_TLOF_GEOMETRY'] = new DDim(842,595);

	
svgdims['009_TLOF_FATO'] = new DDim(842,595);

	
svgdims['010_FATO_PROTECT_ARE'] = new DDim(842,595);

	
svgdims['011_FATO_DIRECTION_L'] = new DDim(842,595);

	
svgdims['012_FATO_DIRECTION_D'] = new DDim(842,595);

	
svgdims['013_TLOF_SAFE_AREA'] = new DDim(842,595);
	
	
svgdims['014_TWY'] = new DDim(842,595);

	
svgdims['015_APRON'] = new DDim(842,595);

	
svgdims['016_APRON_GATE_STAND'] = new DDim(842,595);

	
svgdims['017_AD_HP_SURFACES'] = new DDim(842,595);

	
svgdims['018_SURFACE_CHARACTE'] = new DDim(842,595);

	
svgdims['019_SURFACE_LIGHT_SY'] = new DDim(842,595);

	
svgdims['020_AERO_GND_LGT'] = new DDim(842,595);

	
svgdims['021_ILS'] = new DDim(842,595);

	
svgdims['022_MLS'] = new DDim(842,595);

	
svgdims['023_OBSTACLE'] = new DDim(842,595);

	
svgdims['024_TWY_INTERSECTION'] = new DDim(842,595);

	
svgdims['025_DME'] = new DDim(842,595);

	
svgdims['026_NDB'] = new DDim(842,595);

	
svgdims['027_MKR'] = new DDim(842,595);

	
svgdims['028_TACAN'] = new DDim(842,595);

	
svgdims['029_VOR'] = new DDim(842,595);

	
svgdims['030_AD_HP_NAV_AID'] = new DDim(842,595);

	
svgdims['031_NAVAID_LIMITATIO'] = new DDim(842,595);

	
svgdims['032_DESIGNATED_POINT'] = new DDim(842,595);

	
svgdims['033_SIGNIFICANT_POIN'] = new DDim(842,595);

	
svgdims['034_ANGLE_INDICATION'] = new DDim(842,595);

	
svgdims['035_DISTANCE_INDICAT'] = new DDim(842,595);

	
svgdims['036_SPEC_NAV_SYS'] = new DDim(842,595);

	
svgdims['037_RTE_SEG'] = new DDim(842,595);

	
svgdims['038_RTE_SEG_USE'] = new DDim(842,595);

	
svgdims['039_AIRSPACE'] = new DDim(842,595);

	
svgdims['040_AIRSPACE_VERTEX'] = new DDim(842,595);

	
svgdims['041_AIRSPACE_ASSOC'] = new DDim(842,595);

	
svgdims['042_SID'] = new DDim(842,595);

	
svgdims['043_SID_USAGE'] = new DDim(842,595);

	
svgdims['044_STAR'] = new DDim(842,595);

	
svgdims['045_STAR_USAGE'] = new DDim(842,595);

	
svgdims['046_IAP'] = new DDim(842,595);

	
svgdims['047_PROCEDURE_LEG'] = new DDim(842,595);

	
svgdims['048_MSA_GROUP'] = new DDim(842,595);

	
svgdims['049_ORG_AUTH'] = new DDim(842,595);

	
svgdims['050_UNIT'] = new DDim(842,595);

	
svgdims['051_AUTH_FOR_AIRSPAC'] = new DDim(842,595);

	
svgdims['052_SERVICE'] = new DDim(842,595);

	
svgdims['053_SERVICE_LOCATION'] = new DDim(842,595);

	
svgdims['054_SIGNIFICANT_POIN'] = new DDim(842,595);

	
svgdims['055_SPECIAL_DATE'] = new DDim(842,595);

	
svgdims['056_TFC_FLOW_RESTR'] = new DDim(842,595);

	
svgdims['057_FLOW_COND_COMBIN'] = new DDim(842,595);

	
svgdims['058_TFC_FLOW_RTE'] = new DDim(842,595);

	
svgdims['059_RTE_PORTION'] = new DDim(842,595);

	
svgdims['060_PREDEFINED_LVL'] = new DDim(842,595);

	
svgdims['061_ADDRESS'] = new DDim(842,595);

	
svgdims['062_TIMETABLE'] = new DDim(842,595);

	
svgdims['LARGE_AERODROME'] = new DDim(842,595);

	
svgdims['LARGENAVAIDDESIGPOIN'] = new DDim(1191,842);

	
svgdims['LARGE_SERVICE_AIRSPA'] = new DDim(1191,842);

	
svgdims['LARGE_ROUTE'] = new DDim(1191,842);

	
svgdims['LARGE_PROCEDURES'] = new DDim(1191,842);

